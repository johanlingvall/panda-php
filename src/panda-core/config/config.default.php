<?php
/* =============================================================
 * JLV Framwork
 * https://bitbucket.org/johanlingvall/panda-php
 * =============================================================
 * Copyright 2013 Johan Lingvall
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * ============================================================= */

/* 
 * Framework paths 
 */
$CFG->DOC_ROOT = $_SERVER["DOCUMENT_ROOT"];
$CFG->FRAMEWORK_ROOT = __DIR__ . "/..";
$CFG->SITE_ROOT = __DIR__ . "/../../site";
$CFG->PAGES_ROOT = $CFG->SITE_ROOT . "/pages";
$CFG->DATA_ROOT = $_SERVER["DOCUMENT_ROOT"]."/../data";

// Core settings
$CFG->debug = false;
$CFG->cookie_domain = false; // '.example.com'
$CFG->start_session = true;
$CFG->timezone = false; // 'Europe/Stockholm';
$CFG->url_rules = array();
$CFG->url_vars = array();
$CFG->include_libraries = array();
$CFG->autoload_classes = array();
    
/*
* Database
*/
$CFG->mysql_extension = "mysql"; // or mysqli
$CFG->mysql_host = "";
$CFG->mysql_username = "";
$CFG->mysql_password = "";
$CFG->mysql_db = "";
$CFG->mysql_escape_http_post = true;
$CFG->mysql_escape_http_get = true;
$CFG->mysql_escape_url_vars = true;
    
/* 
 * HTML/Shell
 */
$CFG->shell = "default.php";
$CFG->default_charset = 'utf-8'; // This is also defined in public/.htaccess
?>