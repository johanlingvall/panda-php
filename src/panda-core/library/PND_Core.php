<?php
/* =============================================================
 * JLV Framwork
 * https://bitbucket.org/johanlingvall/panda-php
 * =============================================================
 * Copyright 2013 Johan Lingvall
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * ============================================================= */
 
class PND_Core 
{
	private $page_file;
	private $script_reference;
	
    /*
     * Setting up variables
     */
	public function __construct() {
        global $CFG;        
        
		// Get page uri
		$uri = $this->getUri();
		$basefile = basename($uri);
		if (substr($uri,-1) == "/" || empty($basefile)) {
			$basefile = "index";
			$relative_path = $uri;
		} else {
			$relative_path = substr($uri,0,-strlen($basefile));
		}
        
		if (empty($relative_path)) {
            $relative_path = "/";
		}
        
        $this->script_reference = $relative_path . $basefile;
        $CFG->script_reference = $relative_path . $basefile;
		
		// Set page main file
		$this->page_file = (substr($uri,-4)==".php") 
            ? PAGES_ROOT . $this->script_reference 
            : PAGES_ROOT . $this->script_reference . ".php";
        
            // die($this->page_file);
		if (!file_exists($this->page_file)) {
            $this->page_file = false;
        }
        
		if (!$this->page_file && file_exists(PAGES_ROOT . "/catcher.php")) {
			$this->page_file = PAGES_ROOT . "/catcher.php";
		}
	}
	
    /*
     * Including the main file and the shell
     */
	public function buildPage() {
        global $CFG;

        // Include the main page file
		if ($this->page_file) {
            include($this->page_file);
        }
        
        // Include the shell
		if ($shell_full_path = $this->getShell()) {
            include($shell_full_path);
        }
	}
    
    /*
     * Returns the full path to the current shell
     */
    private function getShell() {
        global $CFG;
        if (!empty($CFG->shell)) {
            $shell_full_path = SITE_ROOT . "/shells/" . basename($CFG->shell);
            return (file_exists($shell_full_path)) ? $shell_full_path : false;
        }
        return false;
    }
	
    /*
     * Executing the view function inside the shell.
     */
    private function fetchView($function_name='') {
        if (!empty($function_name) && function_exists($function_name)) {
            $function_name();
        } else if (function_exists('view')) {
            view();
        }
	}
	
    /*
     * Looping through url rules and returning the current request uri
     */
	private function getUri() {
        global $CFG;
		if (isset($CFG->url_rules) && is_array($CFG->url_rules)) {
            // Check if the current URL matches any of the url_rules expressions
			foreach ($CFG->url_rules as $regex => $uri) {
				if (preg_match($regex,$this->me(),$matches)) {
					$CFG->url_vars = $matches;
                    if ($CFG->mysql_escape_url_vars == true) {
                        foreach($CFG->url_vars as $key => $value) {
                            $CFG->url_vars[$key] = PND_EscapeArray($CFG->url_vars[$key]);
                        }
                    }
					return $uri;
				}
			}
		}
		return $this->me();
	}
	
    /*
     * Fetching request uri
     */
	private function me() {
		$url = (isset($_SERVER['REQUEST_URI'])) ? $_SERVER['REQUEST_URI'] : $_SERVER['PHP_SELF'];
        return ($commapos = strpos($url, '?')) ? substr($url, 0, $commapos) : $url;
	}
}
?>