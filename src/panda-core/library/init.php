<?php
/* =============================================================
 * JLV Framwork
 * https://bitbucket.org/johanlingvall/panda-php
 * =============================================================
 * Copyright 2013 Johan Lingvall
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * ============================================================= */
 

/* 
 * Inport default configuration file
 */
if (isset($CFG->FRAMEWORK_ROOT)) {
    require_once($CFG->FRAMEWORK_ROOT . "/config/config.default.php");
} else {
    require_once($_SERVER["DOCUMENT_ROOT"] . "/../panda-core/config/config.default.php");
}

/*
 * Inport custom configuration file
  */
if (isset($CFG->CONFIG_FILE)) {
    require_once($CFG->CONFIG_FILE);
} else {
    require_once($_SERVER["DOCUMENT_ROOT"] . "/../site/config.php");
}

/* 
 * Set cookie domain
 */
if ($CFG->cookie_domain) {
    session_set_cookie_params(0, '/', $CFG->cookie_domain);
}

/* 
 * Start the session
 */
if ($CFG->start_session) {
    session_start();
}

/* 
 * Set time zone
 */
if ($CFG->timezone) {
    date_default_timezone_set($CFG->timezone);
}

/* 
 * Set charset
 */
if (isset($CFG->default_charset) && !empty($CFG->default_charset)) {
    header("Content-Type: text/html; charset=" . $CFG->default_charset);
    ini_set('default_charset', $CFG->default_charset);
}

/* 
 * Define paths used in the application
 */
define("DOC_ROOT",$CFG->DOC_ROOT);
define("FRAMEWORK_ROOT",$CFG->FRAMEWORK_ROOT);
define("PUBLIC_ROOT",$CFG->PUBLIC_ROOT);
define("SITE_ROOT",$CFG->SITE_ROOT);
define("PAGES_ROOT",$CFG->PAGES_ROOT);
define("DATA_ROOT",$CFG->DATA_ROOT);

    
/*
 * Set error reporting
 */
if ($CFG->debug) {
    $CFG->debug_data = "";
    ini_set("display_errors",1);
    error_reporting(E_ALL);
}

/*
 * Connect to db
 */
$DB = new stdClass();
if (isset($CFG->mysql_host) && !empty($CFG->mysql_host)) {
    if ($CFG->mysql_extension == 'mysql') {
        mysql_connect($CFG->mysql_host, $CFG->mysql_username, $CFG->mysql_password);
        mysql_select_db($CFG->mysql_db);
    } else if ($CFG->mysql_extension == 'mysqli') {
        $DB = new mysqli($CFG->mysql_host, $CFG->mysql_username, $CFG->mysql_password, $CFG->mysql_db);
        $DB->set_charset(str_replace('-', '', $CFG->default_charset));
    }
}

/*
 * Import framework libraries and classes
 */
require_once(FRAMEWORK_ROOT . "/library/PND_Core.php");
require_once(FRAMEWORK_ROOT."/library/PND_lib.php");

/*
 * Import custom libraries and classes
 */
@include_once(SITE_ROOT."/library/init.php");

/*
 * Escape input variables
 */
if ($CFG->mysql_escape_http_post && isset($_POST) && count($_POST)) {
    $_POST = PND_EscapeArray($_POST);
}
if ($CFG->mysql_escape_http_get && isset($_GET) && count($_GET)) {
    $_GET = PND_EscapeArray($_GET);
}
?>